﻿using System.Collections.Generic;

namespace lab3.ViewModels
{
   public struct UserCity
    {
        public int Code { get; set;}
        public string City { get; set;}
    }
    public class UserCallsVm
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }

        public IEnumerable<UserCity> Citys { get; set; }
    }
}