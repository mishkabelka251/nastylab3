using System.Collections.Generic;

namespace lab3.Models
{
    public class City
    {
        public int id { get; set; }
        public float price { get; set; }
        public string city { get; set; }
        public int code { get; set; }
        public List<Calls> calls { get; set; }
    }
}